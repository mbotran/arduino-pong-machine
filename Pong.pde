float ballX;
float ballY;
float speedX; //Speed at which ball moves on x axis. The screen is a 500 by 500 pixel grid broken P1_up into 50 by 50 pixel boxes.
//This means the screen is really 10 by 10 boxes, and the ball moves 1 block per loop.
float speedY;

float P1_PaddleX;
float P1_PaddleY;

float P2_PaddleX; //Width of canvas minus 1/2 block size
float P2_PaddleY;

boolean P1_up, P1_down;
boolean P2_up, P2_down;
boolean P1WonLast;

float P1_score = 0;
float P2_score = 0;


void setup() { //This runs once and creats a 500 by 500 pixel canvas
  rectMode(CENTER); //Everything will measure from the center of a cube
  size(700, 300);
  ballX = width/2;
  ballY = height/2;
  if(P1WonLast == true){
    speedX = 50;
  }
  else if(P1WonLast == false){
    speedX= -50;
  }
  speedY = 50;
  P1_PaddleX = 50/2;
  P1_PaddleY = height/2;
  P2_PaddleX = width - 50/2; 
  P2_PaddleY = height/2;  
  frameRate(6);
}
 
void draw() { //This gets called 60 times a second. 
  background(0); //This makes the background black so we don't see any ghosting from moving sprites
  fill(255, 0, 0); //This fills in the ball as red
  rect(ballX, ballY, 50, 50); //This makes the ball a square
  rect(P1_PaddleX, P1_PaddleY, 50, 150); //This makes one paddle
  rect(P2_PaddleX, P2_PaddleY, 50, 150); //This makes the other paddle

  ballTouchWall();
  ballX = ballX + speedX;
  ballY = ballY + speedY;
  movePaddle(); //Calls movePaddle method
  restrictPaddle();
  contactPaddle();
}

void ballTouchWall(){
 if( ballX > width){
   P1_score = P1_score + 1;
   System.out.println("Player 1 Score: " + String.valueOf(P1_score));
   P1WonLast = true;
   setup(); //the ball touched the right side of the screen so we restart the ball in the center.
   speedX = -speedX;
 }
 else if( ballX < 0){
   P2_score = P2_score + 1;
   P1WonLast = false;
   System.out.println("Player 2 Score: " + String.valueOf(P2_score));
   setup(); //the ball touched the left side of the screen so we restart the ball in the center.
   speedX = -speedX;
 }
 
 if( ballY > height){
   speedY = -speedY;
 }
 else if( ballY < 0){
   speedY = -speedY;
 }
}

void movePaddle(){
  if( P1_up == true){
    P1_PaddleY = P1_PaddleY - 50;
  }
  if( P1_down == true){
    P1_PaddleY = P1_PaddleY + 50;
  }
  if( P2_up == true){
    P2_PaddleY = P2_PaddleY - 50;
  }
  if( P2_down == true){
    P2_PaddleY = P2_PaddleY + 50;
  }
}

void contactPaddle() {
  if (ballX - 50/2 < P1_PaddleX + 50/2 && ballY - 50/2 < P1_PaddleY + 50/2 && ballY + 50/2 > P1_PaddleY - 50/2){
     speedX = -speedX;
  }
  if (ballX + 50/2 > P2_PaddleX - 50/2 && ballY - 50/2 < P2_PaddleY + 50/2 && ballY + 50/2 > P2_PaddleY - 50/2){
     speedX = -speedX;
  }
}
  

void restrictPaddle() { //This makes sure the paddle does not go off screen.
  if(P1_PaddleY - 150/3 < 0){
    P1_PaddleY = P1_PaddleY + 50;
  }
  if(P1_PaddleY + 150/3> height){
    P1_PaddleY = P1_PaddleY - 50;
  }
  if(P2_PaddleY - 150/3 < 0){
    P2_PaddleY = P2_PaddleY + 50;
  }
  if(P2_PaddleY + 150/3> height){
    P2_PaddleY = P2_PaddleY - 50;
  }
}
  

void keyPressed() {
  if(key == 'w' || key == 'W'){
    P1_up = true;
  }  
  else if(key == 's' || key == 'S'){
    P1_down = true;
  }
  if(key == 'o' || key == 'O'){
    P2_up = true;
  }  
  else if(key == 'l' || key == 'L'){
    P2_down = true;
  }
}

void keyReleased() {
  if(key == 'w' || key == 'W'){
    P1_up = false;
  }  
  else if(key == 's' || key == 'S'){
    P1_down = false;
  }  
  if(key == 'o' || key == 'O'){
    P2_up = false;
  }  
  else if(key == 'l' || key == 'L'){
    P2_down = false;
  }
}
